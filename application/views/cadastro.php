<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title>Rapido - Responsive Admin Template</title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="assets/plugins/animate.css/animate.min.css">
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
		<link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
		<link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.theme.css">
		<link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.transitions.css">
		<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote.css">
		<link rel="stylesheet" href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
		<link rel="stylesheet" href="assets/plugins/toastr/toastr.min.css">
		<link rel="stylesheet" href="assets/plugins/bootstrap-select/bootstrap-select.min.css">
		<link rel="stylesheet" href="assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
		<link rel="stylesheet" href="assets/plugins/DataTables/media/css/DT_bootstrap.css">
		<link rel="stylesheet" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
		<link rel="stylesheet" href="assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CORE CSS -->
		<link rel="stylesheet" href="assets/css/styles.css">
		<link rel="stylesheet" href="assets/css/styles-responsive.css">
		<link rel="stylesheet" href="assets/css/plugins.css">
		<link rel="stylesheet" href="assets/css/themes/theme-default.css" type="text/css" id="skin_color">
		<link rel="stylesheet" href="assets/css/print.css" type="text/css" media="print"/>
		<!-- end: CORE CSS -->
		<link rel="shortcut icon" href="favicon.ico" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<div class="main-content">
			
			<!-- /.modal -->
			<!-- end: SPANEL CONFIGURATION MODAL FORM -->
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<!-- start: FORM WIZARD PANEL -->
						<div class="panel panel-white">
							<div class="panel-heading">
								<h4 class="panel-title">Formulário <span class="text-bold">Vaga Programador</span></h4>
								
							</div>
							<div class="panel-body">
								<form action="#" role="form" class="smart-wizard form-horizontal" id="form">
									<div id="wizard" class="swMain">
										<ul>
											<li>
												<a href="#step-1">
													<div class="stepNumber">
														1
													</div>
													<span class="stepDesc"> Passo 1
														<br />
														<small>Nome/Data Nascimento</small> </span>
												</a>
											</li>
											<li>
												<a href="#step-2">
													<div class="stepNumber">
														2
													</div>
													<span class="stepDesc"> Passo 2
														<br />
														<small>Endereço</small> </span>
												</a>
											</li>
											<li>
												<a href="#step-3">
													<div class="stepNumber">
														3
													</div>
													<span class="stepDesc"> Passo 3
														<br />
														<small>Contato</small> </span>
												</a>
											</li>
											<li>
												<a href="#step-4">
													<div class="stepNumber">
														4
													</div>
													<span class="stepDesc"> Step 4
														<br />
														<small>Step 4 description</small> </span>
												</a>
											</li>
										</ul>
										<div class="progress progress-xs transparent-black no-radius active">
											<div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
												<span class="sr-only"> 0% Complete (success)</span>
											</div>
										</div>
										<div id="step-1">
											<h2 class="StepTitle">Passo 1</h2>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Nome Completo <span class="symbol required"></span>
												</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="full_name" name="full_name" placeholder="Nome Completo">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Data de Nascimento(DD/MM/YYYY) <span class="symbol required"></span>
												</label>
												<div class="col-sm-4">
													<div class="row">
														<div class="col-sm-4">
															<input type="text" class="form-control" name="date_dd" id="date_dd" placeholder="DD">
														</div>
														<div class="col-sm-4">
															<select class="form-control" id="date_mm" name="date_mm">
																<option value="">MM</option>
																<option value="01">Janeiro</option>
																<option value="02">Fevereiro </option>
																<option value="03">Março</option>
																<option value="04">Abril</option>
																<option value="05">Maio</option>
																<option value="06">Junho</option>
																<option value="07">Julho</option>
																<option value="08">Agosto</option>
																<option value="09">Setembro</option>
																<option value="10">Outubro</option>
																<option value="11">Novembro</option>
																<option value="12">Dezembro</option>
															</select>
														</div>
														<div class="col-sm-4">
															<input type="text" class="form-control" name="date_yyyy" id="date_yyyy" placeholder="YYYY">
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2 col-sm-offset-8">
													<button class="btn btn-blue next-step btn-block">
														Próximo <i class="fa fa-arrow-circle-right"></i>
													</button>
												</div>
											</div>
										</div>
										<div id="step-2">
											<h2 class="StepTitle">Endereço</h2>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Address <span class="symbol required"></span>
												</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="address" name="address" placeholder="Endereço">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Number <span class="symbol required"></span>
												</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="number" maxlength="10" name="number" placeholder="Número">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Country <span class="symbol required"></span>
												</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="country" name="country" placeholder="Cidade">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													City <span class="symbol required"></span>
												</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="city" name="city" placeholder="Cidade">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													CEP <span class="symbol required"></span>
												</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="cep" name="cep" placeholder="CEP">
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2 col-sm-offset-3">
													<button class="btn btn-light-grey back-step btn-block">
														<i class="fa fa-circle-arrow-left"></i> Back
													</button>
												</div>
												<div class="col-sm-2 col-sm-offset-3">
													<button class="btn btn-blue next-step btn-block">
														Next <i class="fa fa-arrow-circle-right"></i>
													</button>
												</div>
											</div>
										</div>
										<div id="step-3">
											<h2 class="StepTitle">Contato</h2>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Telefone Fixo <span class="symbol required"></span>
												</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="fixed" name="fixed" placeholder="Telefone Fixo">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Telefone Celular <span class="symbol required"></span>
												</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="cell" name="cell" placeholder="Telefone Celular">
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2 col-sm-offset-3">
													<button class="btn btn-light-grey back-step btn-block">
														<i class="fa fa-circle-arrow-left"></i> Back
													</button>
												</div>
												<div class="col-sm-2 col-sm-offset-3">
													<button class="btn btn-blue next-step btn-block">
														Next <i class="fa fa-arrow-circle-right"></i>
													</button>
												</div>
											</div>
										</div>
										<div id="step-4">
											<h2 class="StepTitle">Resumo</h2>
											<h3>Nome/Data Nascimento</h3>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Nome:
												</label>
												<div class="col-sm-7">
													<p class="form-control-static display-value" data-display="full_name"></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Data Nascimento:
												</label>
												<div class="col-sm-7">
													<p class="form-control-static display-value" data-display="date"></p>
												</div>
											</div>
											<h3>Endreço</h3>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Endereço:
												</label>
												<div class="col-sm-7">
													<p class="form-control-static display-value" data-display="address"></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Númeo:
												</label>
												<div class="col-sm-7">
													<p class="form-control-static display-value" data-display="number"></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Country:
												</label>
												<div class="col-sm-7">
													<p class="form-control-static display-value" data-display="country"></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													City:
												</label>
												<div class="col-sm-7">
													<p class="form-control-static display-value" data-display="city"></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													CEP:
												</label>
												<div class="col-sm-7">
													<p class="form-control-static display-value" data-display="cep"></p>
												</div>
											</div>
											<h3>Contato</h3>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Telefone Fixo:
												</label>
												<div class="col-sm-7">
													<p class="form-control-static display-value" data-display="fixed"></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">
													Telefone Celular:
												</label>
												<div class="col-sm-7">
													<p class="form-control-static display-value" data-display="cell"></p>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2 col-sm-offset-8">
													<button class="btn btn-success finish-step btn-block">
														Finish <i class="fa fa-arrow-circle-right"></i>
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<!-- end: FORM WIZARD PANEL -->
					</div>
				</div>
				<!-- end: PAGE CONTENT-->
			</div>
			<div class="subviews">
				<div class="subviews-container"></div>
			</div>
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.min.js"></script>
		<script src="assets/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="assets/plugins/jQuery/jquery-1.11.1.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
		<!--<![endif]-->
		<script src="assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
		<script src="assets/plugins/jquery-cookie/jquery.cookie.js"></script>

		<script src="assets/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
		<script src="assets/plugins/jquery-mockjax/jquery.mockjax.js"></script>
		<script src="assets/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
		<script src="assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
		<script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="assets/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
		<script src="assets/js/form-wizard.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CORE JAVASCRIPTS  -->
		<script src="assets/js/main.js"></script>
		<!-- end: CORE JAVASCRIPTS  -->
		<script>
			jQuery(document).ready(function() {
				FormWizard.init();
				
			});
		</script>
	</body>
	<!-- end: BODY -->
</html>