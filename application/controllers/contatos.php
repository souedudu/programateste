<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contatos extends CI_Controller {


   public $contatos;


   /**
    * Get All Data from this method.
    *
    * @return Response
   */
   public function __construct() {
      parent::__construct(); 

      
      $this->load->library('form_validation');
      $this->load->library('session');
      $this->load->model('Contatos_Model');


      $this->contatos = new Contatos_Model;
   }


   /**
    * Display Data this method.
    *
    * @return Response
   */
   public function index()
   {
    $this->load->view('cadastro');
       
   }
  
  


   public function inserir()
   {


        $id = $this->input->post('id');
        $params = $this->input->post('params');
        $params = json_decode($params);
        $dados = array( );
        foreach ($params as $key => $value) {
          $dados[$value->name] = $value->value;
        }
        $dados = (object) $dados;
        
        $data = array(
         
          'full_name' => $dados->full_name,
          'date' => $dados->date_yyyy.'.'.$dados->date_mm.'.'.$dados->date_dd,
          'address' => $dados->address,
          'country' => $dados->country,
          'number' => $dados->number,
          'city' => $dados->city,
          'cep' => $dados->cep,
          'fixed' => $dados->fixed,
          'cell' => $dados->cell

        );


        $retorno = $this->contatos->insert($data,$id);
        echo  '{"say":"ok","id":'.$retorno.'}';

    }


   /**
    * Edit Data from this method.
    *
    * @return Response
   */
   public function edit($id)
   {

        
       $item = $this->contatos->find($id);

       echo json_encode($item);
   }



   /**
    * Delete Data from this method.
    *
    * @return Response
   */
   public function delete($id)
   {
       $item = $this->contatos->delete($id);
       echo  '{"say":"ok"}';
   }
}