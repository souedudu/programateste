<?php


class Contatos_Model extends CI_Model{

    function __construct(){
    
      parent::__construct();
      $this->load->database();
      
    }

    public function get($email=null){
        if(!empty($email)){
          $this->db->like('email', $email);
        }
        $this->db->select('*');
        $this->db->select("FORMAT(date,'dd/MM/yyyy') as date", false);

        $this->db->from("contatos");
        $this->db->order_by('nome');
        $query = $this->db->get();
        return $query->result_object();
    }


  


    public function insert($data,$id) 
    {
         
        if(empty($id) || $id == null || $id == 'null'){

            $this->db->insert('cadastro',$data);
            return $this->db->insert_id();

        }else{
            $this->db->where('id',$id);
            $this->db->update('cadastro',$data);
            return $id;
        }        
    }


    public function find($id)
    {
        $this->db->select('*');
        $this->db->select("FORMAT(date,'dd') as date_dd", false);
        $this->db->select("FORMAT(date,'MM') as date_mm", false);
        $this->db->select("FORMAT(date,'yyyy') as date_yyyy", false);
        return $this->db->get_where('cadastro', array('id' => $id))->row();
    }


    public function delete($id)
    {
        return $this->db->delete('cadastro', array('id' => $id));
    }
}
?>