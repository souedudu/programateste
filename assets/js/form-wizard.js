
 
var FormWizard = function () {
	"use strict";
    var wizardContent = $('#wizard');
    var wizardForm = $('#form');
    var numberOfSteps = $('.swMain > ul > li').length;
    var initWizard = function () {
        // function to initiate Wizard Form
        wizardContent.smartWizard({
            selected: 0,
            keyNavigation: false,
            onLeaveStep: leaveAStepCallback,
            onShowStep: onShowStep,
        });
        pegarDados();
        var numberOfSteps = 0;
        animateBar();
        initValidator();
    };
    var animateBar = function (val) {
        if ((typeof val == 'undefined') || val == "") {
            val = 1;
        };
        
        var valueNow = Math.floor(100 / numberOfSteps * val);
        $('.step-bar').css('width', valueNow + '%');
    };
    var validateCheckRadio = function (val) {
        $("input[type='radio'], input[type='checkbox']").on('ifChecked', function(event) {
			$(this).parent().closest(".has-error").removeClass("has-error").addClass("has-success").find(".help-block").remove().end().find('.symbol').addClass('ok');
		});
    };    
    var initValidator = function () {
        $("#fixed").mask("(099) 9999-9999");
        $("#cell").mask("(099) 99999-9999");
        $("#cep").mask("99999-999");
        $("#date_dd").mask("99");
        $("#date_yyyyy").mask("9999");

        $.validator.addMethod("cardExpiry", function () {
            //if all values are selected
            if ($("#date_mm").val() != "" && $("#date_dd").val() != "" && $("#date_dd").val() <= 31 && $("#date_yyyy").val() != "") {
                return true;
            } else {
                return false;
            }
        }, 'Por Favor preencha dia, mês e ano válidos');
        $.validator.setDefaults({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "date_mm" || element.attr("name") == "date_yyyy") {
                    error.appendTo($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: ':hidden',
            rules: {
                username: {
                    minlength: 2,
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    minlength: 6,
                    required: true
                },
                password_again: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                full_name: {
                    required: true,
                    minlength: 2,
                },
                phone: {
                    required: true
                },
                address: {
                    required: true
                },
                number: {
                    required: true
                },
                city: {
                    required: true
                },
                cep: {
                    required: true
                },
                country: {
                    required: true
                },
                fixed: {
                    required: true
                },
                cell: {
                    required: true
                },

                date_yyyy: "cardExpiry"
                
            },
            messages: {
                firstname: "Please specify your first name"
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            }
        });
    };
    var displayConfirm = function () {
        $('.display-value', form).each(function () {
            var input = $('[name="' + $(this).attr("data-display") + '"]', form);
            if (input.attr("type") == "text" || input.attr("type") == "email" || input.is("textarea")) {
                $(this).html(input.val());
            } else if (input.is("select")) {
                $(this).html(input.find('option:selected').text());
            } else if (input.is(":radio") || input.is(":checkbox")) {

                $(this).html(input.filter(":checked").closest('label').text());
            } else if ($(this).attr("data-display") == 'date') {
                var data = $('[name="date_dd"]', form).val() + '/' + $('[name="date_mm"]', form).val() + '/' +$('[name="date_yyyy"]', form).val();
                $(this).html(  data );
            }
        });
    };
    var gravarCookie = function () {
        $.each($('#form').serializeArray(),function(index, el) {
            var input = $(el);

                $.cookie(input[0].name, input[0].value);
        });

    }
    var salvarDados = function(){
        var dados = $('#form').serializeArray();
        var jsonValue = JSON.stringify(dados, null, " ");
        $.ajax({
            url: 'contatos/inserir',
            method: "POST",
            dataType: "json",
            data: {
                nocache: new Date().getTime(),
                params: jsonValue,
                'id':$.cookie("cookieid")

            },
        }).done(function(result) {
            if (result.say == 'ok'){

                $.cookie('cookieid', result.id);
                
            }

        });
    }
    var pegarDados = function(){
        if ($.cookie("cookieid") == "null") return false;
        var dados = $('#form').serializeArray();
        var jsonValue = JSON.stringify(dados, null, " ");
        $.ajax({
            url: 'contatos/edit/'+$.cookie("cookieid"),
            method: "POST",
            dataType: "json",
            data: {
                nocache: new Date().getTime()

            },
        }).done(function(result) {
            $.each(result,function(index, el) {
                var input = $('[name="' + index + '"]');
                input.val(el);
            });

        });
    }
    var restaurarCookie = function () {
       



        //aqui seria para não ter que pegar os dados por banco de dados e sim com cooke

        $.each($('#form').serializeArray(),function(index, el) {
             var input = $('[name="' + $(el)[0].name + '"]', form);
            if (input.attr("type") == "text" || input.attr("type") == "email" || input.is("textarea")) {
                input.val($.cookie($(el)[0].name));
            } else if (input.is("select")) {
                input.val($.cookie($(el)[0].name));
                // $(this).html(input.find('option:selected').text());
            } else if (input.is(":radio") || input.is(":checkbox")) {

              //  $(this).html(input.filter(":checked").closest('label').text());
            } else if ($(this).attr("data-display") == 'date') {
               //$(this).html($('[name="date_dd"]', form).val()) + '/' + $(this).html($('[name="date_mm"]', form).val() + '/' + $('[name="date_yyyy"]', form).val());
            }
        });

    }

    var onShowStep = function (obj, context) {
    	if(context.toStep == numberOfSteps){
    		$('.anchor').children("li:nth-child(" + context.toStep + ")").children("a").removeClass('wait');
            displayConfirm();
        }
        $(".next-step").unbind("click").click(function (e) {
            e.preventDefault();
            salvarDados();
            //gravarCookie();
            wizardContent.smartWizard("goForward");
        });
        $(".back-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goBackward");
        });
        $(".finish-step").unbind("click").click(function (e) {
            e.preventDefault();
            salvarDados();
            $.cookie('cookieid', 0);
            onFinish(obj, context);
        });
    };
    var leaveAStepCallback = function (obj, context) {
        return validateSteps(context.fromStep, context.toStep);
        // return false to stay on step and true to continue navigation
    };
    var onFinish = function (obj, context) {

        if (validateAllSteps()) {
            $.cookie('cookieid', 0);
            window.location.reload();
        }
    };
    var validateSteps = function (stepnumber, nextstep) {
        var isStepValid = false;
        
        
        if (numberOfSteps >= nextstep && nextstep > stepnumber) {
        	
            // cache the form element selector
            if (wizardForm.valid()) { // validate the form
                wizardForm.validate().focusInvalid();
                for (var i=stepnumber; i<=nextstep; i++){
        		$('.anchor').children("li:nth-child(" + i + ")").not("li:nth-child(" + nextstep + ")").children("a").removeClass('wait').addClass('done').children('.stepNumber').addClass('animated tada');
        		}
                //focus the invalid fields
                animateBar(nextstep);
                isStepValid = true;
                return true;
            };
        } else if (nextstep < stepnumber) {
        	for (i=nextstep; i<=stepnumber; i++){
        		$('.anchor').children("li:nth-child(" + i + ")").children("a").addClass('wait').children('.stepNumber').removeClass('animated tada');
        	}
            
            animateBar(nextstep);
            return true;
        } 
    };
    var validateAllSteps = function () {
        var isStepValid = true;
        // all step validation logic
        return isStepValid;
    };
    return {
        init: function () {
            initWizard();
            validateCheckRadio();
        }
    };
}();